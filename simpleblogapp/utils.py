

def pages_for_pagination(current_page,
                         number_of_all_pages,
                         number_of_pages_for_show=5):
    # number_of_pages_for_show повинно бути не парним
    list_of_pages = []

    # перевіряємо чи кількість сторінок для показу
    # не перевищує загальну кількість сторінок
    if number_of_all_pages <= number_of_pages_for_show:
        for number in range(1, number_of_all_pages+1):
            list_of_pages.append(number)

    elif current_page <= number_of_pages_for_show//2:
        for number in range(1, (number_of_pages_for_show + 1)):
            list_of_pages.append(number)

    elif current_page >= (number_of_all_pages - (number_of_pages_for_show//2-1)):
        for number in range((number_of_all_pages-(number_of_pages_for_show-1)),
                            (number_of_all_pages+1)):
            list_of_pages.append(number)
    else:
        for number in range(current_page-(number_of_pages_for_show//2),
                            current_page+(number_of_pages_for_show//2+1)):
            list_of_pages.append(number)
    return list_of_pages
