from django import forms


class CommentForm(forms.Form):
    comment = forms.CharField(label='comment', max_length=300)


class RegistrationForm(forms.Form):
    name = forms.CharField(label='name', max_length=10)
    email = forms.EmailField(label='email')
    password = forms.CharField(label='password', max_length=20)
    first_name = forms.CharField(label='first_name', max_length=15)
    last_name = forms.CharField(label='last_name', max_length=15)
    country = forms.CharField(label='country', max_length=15)
    city = forms.CharField(label='city', max_length=10)
    birthday = forms.DateField(label='birthday')
