# Generated by Django 2.0.3 on 2018-03-16 11:03

from django.db import migrations
def add_data(apps, schema_editor):
    Article = apps.get_model("simpleblogapp", "Article")
    db_alias = schema_editor.connection.alias
    body_text = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32 The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'


    Article.objects.using(db_alias).bulk_create([
        Article(id=17,
                name='Lorem Ipsum has been the',
                description='These article about holidays',
                body_text=body_text,
                image='articles/pic09.jpg'),
    ])

class Migration(migrations.Migration):

    dependencies = [
        ('simpleblogapp', '0006_auto_20180316_1045'),
    ]

    operations = [
        migrations.RunPython(
            add_data,
        )]
