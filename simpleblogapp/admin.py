from django.contrib import admin

from simpleblogapp.models import Article, Tags, Comments

admin.site.register(Article)
admin.site.register(Tags)
admin.site.register(Comments)
